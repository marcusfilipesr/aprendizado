import numpy as np
from lmest_rotor.post_processing import PostProcessing
from rotor import mount_rotor

rotor = mount_rotor()

massunbt = [1e-4, 0]
phaseunbt = [-np.pi / 2, 0]

probe1 = 14
probe2 = 22

x = rotor.run(0, 0.0001, 18, massunbt, phaseunbt, 1200, print_progress=False)

results = PostProcessing(rotor, [probe1, probe2], x)

fig_probe1_time = results.plot_time_response()[0].show()
fig_probe2_time = results.plot_time_response()[1].show()
