from abc import ABC, abstractmethod


class Abs_bearing(ABC):
    """Class to create abstract bearing mandatory properties. This
    is intended to be used for defining the standardized bearing
    nodal matrices and requirements.
    """

    def __init__(self):
        pass

    @abstractmethod
    def K(self):
        pass

    @abstractmethod
    def KbearingEst(self):
        pass

    @abstractmethod
    def C(self):
        pass
