from abc import ABC, abstractmethod


class Abs_Element(ABC):
    """Class to create abstract element mandatory properties. This is
    intended to be used for defining the standardized shaft element
    matrices and requirements.
    """

    def __init__(self):
        pass

    @abstractmethod
    def K(self):
        pass

    @abstractmethod
    def Kst(self):
        pass

    @abstractmethod
    def C(self):
        pass

    @abstractmethod
    def G(self):
        pass

    @abstractmethod
    def M(self):
        pass
