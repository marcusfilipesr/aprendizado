import json
from os.path import dirname, join


class Material:
    """Material
    Class to create materials database.

    Parameters
    ----------
    E import json: float
        Young modulus, [Pa]
    nu : float
        Poisson coefficient, [-]
    rho : float
        Volumetric density, [kg/m^3]
    name : string, optional
        Material name, by default None
    pcolor : string, optional
        Material color for plots, by default #003366

    Examples
    -------
    >>> from lmest_rotor import Material

    ### create material object
    >>> steel = Material(E=211e9, nu=0.3, rho=7810, name="Steel", pcolor="#003366")
    >>> steel
    {'E': 211000000000.0, 'nu': 0.3, 'G': 81153846153.84615, 'rho': 7810, 'name': 'STEEL', 'pcolor': '#003366'}

    ### create material from data in data file
    >>> steel = Material.from_data("steel")
    >>> steel
    {'E': 211000000000.0, 'nu': 0.3, 'G': 81153846153.84615, 'rho': 7810, 'name': 'STEEL', 'pcolor': '#003366'}

    ### list materials in data file
    >>> Material.list_availables()
    STEEL............... {'E': 211000000000.0, 'nu': 0.3, 'G': 81153846153.84615, 'rho': 7810, 'pcolor': '#003366'}

    ### remove material from data file
    >>> Material.delete("steel")

    ### save data of material in data file
    >>> steel = Material(E=211e9, nu=0.3, rho=7810, name="Steel", pcolor="#003366")
    >>> steel.save()



    """

    _file = join(dirname(__file__), "materials.json")

    def __init__(self, E, nu, rho, name="", pcolor="#003366"):
        # properties list for an ISOTROPIC material
        self.E = E
        self.nu = nu
        self.G = E / ((1 + nu) * 2)
        self.rho = rho
        self.name = name.upper()
        self.pcolor = pcolor

    def __str__(self):
        # returns a string describing the material and its properties
        return (
            f"Material description \n"
            f"  Name = {self.name} \n"
            f"  Plot color = {self.pcolor} \n"
            f"  Young modulus = {self.E}[Pa] \n"
            f"  Shear modulus = {self.G}[Pa] \n"
            f"  Poisson coefficient = {self.nu}[ - ] \n"
            f"  Volumetric density = {self.rho}[kg/m^3] \n"
        )

    def __repr__(self):
        return str(self.__dict__)

    def _can_be_saved(self):
        material = self.__dict__
        return all(material.values())

    @classmethod
    def _load(cls):
        try:
            with open(cls._file, "r") as data_file:
                data = json.loads(data_file.read())
        except FileNotFoundError:
            data = {}

        return data

    @classmethod
    def _dump(cls, data):
        with open(cls._file, "w") as data_file:
            json.dump(data, data_file, indent=4)

    def save(self):
        """Saves material in data file.

        Example:
        --------
        >>> from lmest_rotor import Material
        >>> steel = Material(E=211e9, nu=0.3, rho=7810, name="Steel", pcolor="#003366")
        >>> steel.save()
        """

        if self._can_be_saved():
            data = self._load()
            data[self.name] = self.__dict__.copy()
            del data[self.name]["name"]

            sorted_data = dict(sorted(data.items(), key=lambda x: x[0]))
            self._dump(sorted_data)
        else:
            print("\033[31mNot saved: you should define a name.\033[m")

    @classmethod
    def delete(cls, name: str):
        """Delete a material from the data file

        Parameters
        ----------
        name : str
            Name of material in data file.

        Example:
        --------
        >>> from lmest_rotor import Material
        >>> Material.delete("steel")
        """
        name = name.upper()
        data = cls._load()
        del data[name]
        cls._dump(data)

    @classmethod
    def list_availables(cls):
        """lists saved materials

        Example:
        --------
        >>> from lmest_rotor import Material
        >>> Material.list_availables()

        """

        data = cls._load()
        for key, value in data.items():
            print(f"{key:.<20} {value}")

    @classmethod
    def from_data(cls, name: str):
        """create Material from data in data file.

        Parameters
        ----------
        name : str
            Name of material in data file.

        Returns
        -------
        Material object

        Example:
        --------
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> print(steel))
        Material description
          Name = STEEL
          Plot color = #003366
          Young modulus = 211000000000.0[Pa]
          Shear modulus = 81153846153.84615[Pa]
          Poisson coefficient = 0.3[ - ]
          Volumetric density = 7810[kg/m^3]
        """
        name = name.upper()
        data = cls._load()
        material_data = data[name]
        material_data["name"] = name
        del material_data["G"]
        return cls(**material_data)


if __name__ == "__main__":
    steel = Material(E=211e9, nu=0.3, rho=7810, name="bbb", pcolor="#003366")
    steel.save()
