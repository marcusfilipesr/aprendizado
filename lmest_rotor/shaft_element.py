import numpy as np

from . import Abs_Element, Material


class ShaftElement6DoF(Abs_Element):
    r"""A 6 Degrees of Freedom shaft element.
    =========================================
    This class will create a shaft element that takes into account shear stress, rotary inertia
    and gyroscopic effects. The matrices will be defined considering the following local
    coordinate vector:

    \begin{equation}
    [u1, v1, w1, \theta1, \psi1, \phi1, u2, v2, w2, \theta2, \psi2, \phi2]**T
    \end{equation}

    Being the following their ordering for an element:
    \begin{equation}
        :math:`u`      - horizontal translation;
        :math:`v`      - axial translation;
        :math:`w`      - vertical translation;
        :math:`\theta` - rotation around horizontal, bending on the zy plane;
        :math:`\psi`   - torsion around axial, y direction.
        :math:`\phi`   - rotation around vertical, bending on the xy plane;
    \end{equation}

    Parameters
    ----------
    L : float
        Element length [m].
    id : float
        Inner diameter of the element [m].
    od : float
        Outer diameter of the element [m].
    material : rotor-dynamics.material
        Shaft material.
    alpha : float
        Proportional damping coefficient, associated to the element Mass matrix
    beta : float
        Proportional damping coefficient, associated to the element Stiffness matrix

    Returns
    -------
    A 6 degrees of freedom shaft element, with available gyroscopic, shear and rotary inertia effects.

    Attributes
    ----------
    I : float
       Area Moment of Inertia for the element [rad.(m**4)].
    J : float
       Polar moment of inertia for the element [rad.(m**4)].
    S : float
       Cross section area for the element [rad.(m**2)].
    kappa : float
        Shear coefficient for the element, determined from :cite:`Hutchingson2001` formulation.

    References
    ----------
    .. bibliography:: ../../../docs/refs.bib

    Examples
    --------
    >>> import lmest_rotor as lm
    >>> from lmest_rotor import Material
    >>> steel = Material.from_data("steel")
    >>> Shaft_Element = lm.ShaftElement6DoF(
    ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
    ...                         )
    >>> Shaft_Element.I
    4.9087385212340526e-06
    """

    # @check_units
    def __init__(
        self,
        L,
        id,
        od,
        material,
        alpha,
        beta,
    ):
        self.DOF = 6
        # Pick the proper material from the material json or from the direct entry
        if type(material) is str:
            self.material = Material.from_data(material)
        else:
            self.material = material

        # Attributing properties to their respective variables
        self.L = L
        self.id = id
        self.od = od
        self.color = self.material.pcolor
        self.alpha = float(alpha)
        self.beta = float(beta)

        # Timoshenko kappa factor determination, based on the diameters relation
        if self.__is_circular():  # full circular cross- shaft
            kappa = (6 * (1 + self.material.nu) ** 2) / (
                7 + 12 * self.material.nu + 4 * self.material.nu ** 2
            )
        elif self.__is_thickwall():  # thick-walled cross-section tube shaft
            a = self.id
            b = self.od
            v = self.material.nu
            kappa = (6 * (a ** 2 + b ** 2) ** 2 * (1 + v) ** 2) / (
                7 * a ** 4
                + 34 * a ** 2 * b ** 2
                + 7 * b ** 4
                + v * (12 * a ** 4 + 48 * a ** 2 * b ** 2 + 12 * b ** 4)
                + v ** 2 * (4 * a ** 4 + 16 * a ** 2 * b ** 2 + 4 * b ** 4)
            )
        else:  # thin-walled cross-section tube shaft
            kappa = (1 + self.material.nu) / (2 + self.material.nu)

        self.kappa = kappa

        # Other auxiliary variables - cross-section area and inertia moments
        self.S = np.pi * ((self.od / 2) ** 2 - (self.id / 2) ** 2)
        self.I = np.pi / 4 * ((self.od / 2) ** 4 - (self.id / 2) ** 4)
        self.J = np.pi / 2 * ((self.od / 2) ** 4 - (self.id / 2) ** 4)

    def __is_circular(self):
        # conditional for full circular cross-section
        return self.id == 0

    def __is_thickwall(self):
        # conditional for thick-walled cross-section
        p = (self.od - self.id) / self.od
        return p >= 0.2

    def save(self):
        # saving data function
        pass

    def load(self):
        # loading data function
        pass

    # @property
    def M(self):
        r"""Mass matrix for an instance of a 6 DoF shaft element.

        Returns
        -------
        M: np.ndarray
            Mass matrix for the 6 DoF shaft element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement6DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.M()[:12, :12]
        array(12x12)
        """

        # temporary material and geometrical constants
        L = self.L
        aux1 = self.material.rho * self.S * L / 420
        aux2 = self.material.rho * self.J * L / 6

        # element level matrix declaration
        # fmt: off
        Ms = aux1 * np.array([
            [156        ,0           ,0           ,0           ,0           ,-22*L       ,54          ,0           ,0           ,0           ,0           ,13*L    ],    
            [0          ,140         ,0           ,0           ,0           ,0           ,0           ,70          ,0           ,0           ,0           ,0       ],    
            [0          ,0           ,156         ,22*L        ,0           ,0           ,0           ,0           ,54          ,-13*L       ,0           ,0       ],     
            [0          ,0           ,22*L        ,4*L**2      ,0           ,0           ,0           ,0           ,13*L        ,-3*L**2     ,0           ,0       ],      
            [0          ,0           ,0           ,0           ,2*aux2/aux1 ,0           ,0           ,0           ,0           ,0           ,aux2/aux1   ,0       ],     
            [-22*L      ,0           ,0           ,0           ,0           ,4*L**2      ,-13*L       ,0           ,0           ,0           ,0           ,-3*L**2 ],    
            [54         ,0           ,0           ,0           ,0           ,-13*L       ,156         ,0           ,0           ,0           ,0           ,22*L    ],      
            [0          ,70          ,0           ,0           ,0           ,0           ,0           ,140         ,0           ,0           ,0           ,0       ],       
            [0          ,0           ,54          ,13*L        ,0           ,0           ,0           ,0           ,156         ,-22*L       ,0           ,0       ],      
            [0          ,0           ,-13*L       ,-3*L**2     ,0           ,0           ,0           ,0           ,-22*L       ,4*L**2      ,0           ,0       ],  
            [0          ,0           ,0           ,0           ,aux2/aux1   ,0           ,0           ,0           ,0           ,0           ,2*aux2/aux1 ,0       ],
            [13*L       ,0           ,0           ,0           ,0           ,-3*L**2     ,22*L        ,0           ,0           ,0           ,0           ,4*L**2  ],
            ])
    
        Mt = self.material.rho * self.I / ( 30 * L ) * np.array([
            [36         ,0           ,0           ,0           ,0           ,-3*L        ,-36         ,0           ,0           ,0           ,0           ,-3*L    ],   
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],    
            [0          ,0           ,36          ,3*L         ,0           ,0           ,0           ,0           ,-36         ,3*L         ,0           ,0       ],   
            [0          ,0           ,3*L         ,4*L**2      ,0           ,0           ,0           ,0           ,-3*L        ,-L**2       ,0           ,0       ],       
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],     
            [-3*L       ,0           ,0           ,0           ,0           ,4*L**2      ,3*L         ,0           ,0           ,0           ,0           ,-L**2   ],     
            [-36        ,0           ,0           ,0           ,0           ,3*L         ,36          ,0           ,0           ,0           ,0           ,3*L     ],  
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],
            [0          ,0           ,-36         ,-3*L        ,0           ,0           ,0           ,0           ,36          ,-3*L        ,0           ,0       ],    
            [0          ,0           ,3*L         ,-L**2       ,0           ,0           ,0           ,0           ,-3*L        ,4*L**2      ,0           ,0       ],        
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],    
            [-3*L       ,0           ,0           ,0           ,0           ,-L**2       ,3*L         ,0           ,0           ,0           ,0           , 4*L**2 ],
            ])
        # fmt: on

        M = Ms + Mt
        return M

    def K(self):
        r"""Stiffness matrix for an instance of a 6 DoF shaft element.

        Returns
        -------
        K: np.ndarray
            Omega independent stiffness matrix for the 6 DoF shaft element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement6DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.K()[:12, :12]
        array(12x12)
        """

        # temporary material and geometrical constants
        L = self.L
        a = (
            12
            * self.material.E
            * self.I
            / (self.material.G * self.kappa * self.S * L ** 2)
        )
        aux1 = self.material.E * self.I / ((1 + a) * L ** 3)
        aux2 = self.material.G * self.J / L
        aux3 = self.material.E * self.S / L

        # element level matrix declaration
        # fmt: off
        Kc = aux1 * np.array([
            [12          ,0           ,0           ,0           ,0           ,-6*L        ,-12         ,0           ,0           ,0           ,0           ,-6*L       ],  
            [0           ,aux3/aux1   ,0           ,0           ,0           ,0           ,0           ,-aux3/aux1  ,0           ,0           ,0           ,0          ],
            [0           ,0           ,12          ,6*L         ,0           ,0           ,0           ,0           ,-12         ,6*L         ,0           ,0          ],  
            [0           ,0           ,6*L         ,(4+a)*L**2  ,0           ,0           ,0           ,0           ,-6*L        ,(2-a)*L**2  ,0           ,0          ],  
            [0           ,0           ,0           ,0           ,aux2/aux1   ,0           ,0           ,0           ,0           ,0           ,-aux2/aux1  ,0          ], 
            [-6*L        ,0           ,0           ,0           ,0           ,(4+a)*L**2  ,6*L         ,0           ,0           ,0           ,0           ,(2-a)*L**2 ],   
            [-12         ,0           ,0           ,0           ,0           ,6*L         ,12          ,0           ,0           ,0           ,0           ,6*L        ],  
            [0           ,-aux3/aux1  ,0           ,0           ,0           ,0           ,0           ,aux3/aux1   ,0           ,0           ,0           ,0          ],  
            [0           ,0           ,-12         ,-6*L        ,0           ,0           ,0           ,0           ,12          ,-6*L        ,0           ,0          ],   
            [0           ,0           ,6*L         ,(2-a)*L**2  ,0           ,0           ,0           ,0           ,-6*L        ,(4+a)*L**2  ,0           ,0          ], 
            [0           ,0           ,0           ,0           ,-aux2/aux1  ,0           ,0           ,0           ,0           ,0           ,aux2/aux1   ,0          ],
            [-6*L        ,0           ,0           ,0           ,0           ,(2-a)*L**2  ,6*L         ,0           ,0           ,0           ,0           ,(4+a)*L**2 ],
            ])
        # fmt: on

        K = Kc
        return K

    def Kf(self):
        r"""Stiffness matrix due to the axial force aplied for an instance at a 6 DoF shaft element.

        Returns
        -------
        Kf: np.ndarray
            Stiffness matrix due to the axial force aplied at the 6 DoF shaft element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement6DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.Kf()[:12, :12]
        array(12x12)
        """
        # Axial force applied to the element.
        L = self.L
        # fmt: off
        Kf = 1 / (30 * L) * np.array([
            [36         ,0           ,0           ,0           ,0           ,-3*L        ,-36         ,0           ,0           ,0           ,0           ,-3*L    ],
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],
            [0          ,0           ,36          ,3*L         ,0           ,0           ,0           ,0           ,-36         ,3*L         ,0           ,0       ],
            [0          ,0           ,3*L         ,4*L**2      ,0           ,0           ,0           ,0           ,-3*L        ,-L**2       ,0           ,0       ],
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],
            [-3*L       ,0           ,0           ,0           ,0           ,4*L**2      ,3*L         ,0           ,0           ,0           ,0           ,-L**2   ], 
            [-36        ,0           ,0           ,0           ,0           ,3*L         ,36          ,0           ,0           ,0           ,0           ,3*L     ],    
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],   
            [0          ,0           ,-36         ,-3*L        ,0           ,0           ,0           ,0           ,36          ,-3*L        ,0           ,0       ],      
            [0          ,0           ,3*L         ,-L**2       ,0           ,0           ,0           ,0           ,-3*L        ,4*L**2      ,0           ,0       ],    
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],    
            [-3*L       ,0           ,0           ,0           ,0           ,-L**2       ,3*L         ,0           ,0           ,0           ,0           ,4*L**2  ],
            ])
        # fmt: on
        return Kf

    def Kt(self):
        r"""Stiffness matrix due to the torque aplied for an instance at a 6 DoF shaft element.

        Returns
        -------
        Kf: np.ndarray
            Stiffness matrix due to the torque aplied at the 6 DoF shaft element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement6DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.Kt()[:12, :12]
        array(12x12)
        """
        L = self.L
        # fmt: off
        Kt = - 1 * np.array([
            [0          ,0           ,0           ,-1/L        ,0           ,0           ,0           ,0           ,0           ,1/L         ,0           ,0       ],
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],
            [0          ,0           ,0           ,0           ,0           ,-1/L        ,0           ,0           ,0           ,0           ,0           ,1/L     ],
            [-1/L       ,0           ,0           ,0           ,0           ,-0.5        ,1/L         ,0           ,0           ,0           ,0           ,0.5     ], 
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],
            [0          ,0           ,-1/L        ,0.5         ,0           ,0           ,0           ,0           ,1/L         ,-0.5        ,0           ,0       ], 
            [0          ,0           ,0           ,1/L         ,0           ,0           ,0           ,0           ,0           ,-1/L        ,0           ,0       ],  
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],
            [0          ,0           ,0           ,0           ,0           ,1/L         ,0           ,0           ,0           ,0           ,0           ,-1/L    ], 
            [1/L        ,0           ,0           ,0           ,0           ,-0.5        ,-1/L        ,0           ,0           ,0           ,0           ,0.5     ],
            [0          ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0       ],
            [0          ,0           ,1/L         ,0.5         ,0           ,0           ,0           ,0           ,-1/L        ,-0.5        ,0           ,0       ],
            ])
        # fmt: on
        return Kt

    def Kst(self):
        r"""Dynamic stiffness matrix for an instance of a 6 DoF shaft element.

        Returns
        -------
        Kst: np.ndarray
            Dynamic stiffness matrix for the 6 DoF shaft element. This is
            directly dependent on the rotation speed Omega. It needs to be
            multiplied by the adequate Omega value when used in time depen-
            dent analyses. The matrix multiplier term is:

            [(Iz*Omega*rho)/(15*L)] * [Kst]

            and here the Omega value has been suppressed and must be added
            in the adequate analyses.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement6DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.Kst()[:12, :12]
        array(12x12)
        """

        # temporary material and geometrical constants
        L = self.L

        # fmt: off
        # dynamic stiffening matrix
        Kst = self.material.rho * self.I / (15 * L) * np.array([
            [0           ,0           ,-36         ,-3*L        ,0           ,0           ,0           ,0           ,36          ,-3*L        ,0           ,0          ],
            [0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0          ],
            [0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0          ],
            [0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0          ],
            [0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0          ],
            [0           ,0           ,3*L         ,4*L**2      ,0           ,0           ,0           ,0           ,-3*L        ,-L**2       ,0           ,0          ],
            [0           ,0           ,36          ,3*L         ,0           ,0           ,0           ,0           ,-36         ,3*L         ,0           ,0          ],
            [0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0          ],
            [0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0          ],
            [0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0          ],
            [0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0           ,0          ],
            [0           ,0           ,3*L         ,-L**2       ,0           ,0           ,0           ,0           ,-3*L        ,4*L**2      ,0           ,0          ],
            ])
        # fmt: on

        return Kst

    def C(self):
        r"""Proportional damping matrix for an instance of a 6 DoF shaft element.

        Returns
        -------
        C: np.ndarray
            Proportional damping matrix for the 6 DoF shaft element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement6DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.C()[:12, :12]
        array(12x12)
        """

        # Proportinal damping matrix
        C = self.alpha * self.M() + self.beta * self.K()

        return C

    def G(self):
        r"""Gyroscopic matrix for an instance of a 6 DoFs shaft element.

        Returns
        -------
        G: np.ndarray
            Gyroscopic matrix for the 6 DoF shaft element. Similar to the Kst
            stiffness matrix, this Gyro matrix is also multiplied by the value
            of the rotating speed Omega. It is omitted from this and must be
            added in the respective analyses.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement6DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.G()[:12, :12]
        array(12x12)
        """

        # temporary material and geometrical constants
        L = self.L
        a = (
            12
            * self.material.E
            * self.I
            / (self.material.G * self.kappa * self.S * L ** 2)
        )
        gcor = (6 / 5) / (L ** 2 * (1 + a) ** 2)
        hcor = -(1 / 10 - 1 / 2 * a) / (L * ((1 + a) ** 2))
        icor = (2 / 15 + 1 / 6 * a + 1 / 3 * a ** 2) / ((1 + a) ** 2)
        jcor = -(1 / 30 + 1 / 6 * a - 1 / 6 * a ** 2) / ((1 + a) ** 2)

        # fmt: off
        # Gyroscopic effect matrix
        # G = 2 * self.material.rho * L * self.I * np.array([
        #     [           0,           0,       -gcor,       -hcor,           0,           0,           0,           0,        gcor,       -hcor,           0,          0],
        #     [           0,           0,           0,           0,           0,           0,           0,           0,           0,           0,           0,          0],
        #     [        gcor,           0,           0,           0,           0,       -hcor,       -gcor,           0,           0,           0,           0,      -hcor],
        #     [        hcor,           0,           0,           0,           0,       -icor,       -hcor,           0,           0,           0,           0,      -jcor],
        #     [           0,           0,           0,           0,           0,           0,           0,           0,           0,           0,           0,          0],
        #     [           0,           0,        hcor,        icor,           0,           0,           0,           0,       -hcor,        jcor,           0,          0],
        #     [           0,           0,       -gcor,       -hcor,           0,           0,           0,           0,       -gcor,        hcor,           0,          0],
        #     [           0,           0,           0,           0,           0,           0,           0,           0,           0,           0,           0,          0],
        #     [        gcor,           0,           0,           0,           0,       -hcor,        gcor,           0,           0,           0,           0,       hcor],
        #     [       -hcor,           0,           0,           0,           0,        jcor,       -hcor,           0,           0,           0,           0,      -icor],
        #     [           0,           0,           0,           0,           0,           0,           0,           0,           0,           0,           0,          0],
        #     [           0,           0,       -hcor,       -jcor,           0,           0,           0,           0,       -hcor,        icor,           0,          0],
        #     ])
        # fmt: on

        # fmt: off
        nncor = np.array([
            [     0,  0, gcor,      hcor,  0,     0],
            [     0,  0,     0,       0,   0,     0],
            [ -gcor,  0,     0,       0,   0,  hcor],
            [ -hcor,  0,     0,       0,   0,  icor],
            [     0,  0,     0,       0,   0,     0],
            [     0,  0, -hcor,   -icor,   0,     0],
        ])
        
        nn1cor = np.array([
            [     0,  0, -gcor,    hcor,   0,     0],
            [     0,  0,     0,       0,   0,     0],
            [  gcor,  0,     0,       0,   0,  hcor],
            [  hcor,  0,     0,       0,   0,  jcor],
            [     0,  0,     0,       0,   0,     0],
            [     0,  0,  hcor,   -jcor,   0,     0],
        ])
            
        mmcor = np.array([
            [     0,     0, -gcor,   -hcor,   0,     0],
            [     0,     0,     0,       0,   0,     0],
            [  gcor,     0,     0,       0,   0, -hcor],
            [ -hcor,     0,     0,       0,   0,  jcor],
            [     0,     0,     0,       0,   0,     0],
            [     0,     0, -hcor,   -jcor,   0,     0],
        ])

        mm1cor = np.array([
            [     0,     0,  gcor,  -hcor,    0,     0],
            [     0,     0,     0,       0,   0,     0],
            [ -gcor,     0,     0,       0,   0, -hcor],
            [  hcor,     0,     0,       0,   0,  icor],
            [     0,     0,     0,       0,   0,     0],
            [     0,     0,  hcor,   -icor,   0,     0],
        ])
        # fmt: on
        aux = 2 * self.material.rho * L * self.I

        # Gyroscopic effect matrix

        hor1 = np.hstack([-nncor, -nn1cor])
        hor2 = np.hstack([-mmcor, -mm1cor])
        G = aux * np.vstack([hor1, hor2])

        return G


class ShaftElement4DoF(Abs_Element):
    r"""A 4 Degrees of Freedom shaft element
       ------------------------------------
    This class will create a shaft element that takes into account shear stress, rotary inertia
    and gyroscopic effects. The matrices will be defined considering the following local
    coordinate vector:

    \begin{equation}
    [u_1, w_1, \theta_1, \phi_1, u_2, w_2, \theta_2, \phi_2]**T
    \end{equation}

    Being the following their ordering for an element:
    ..latex:
    \begin{equation}
        `u`      - horizontal translation;
        `w`      - vertical translation;
        `\theta` - rotation around horizontal, bending on the zy plane;
        `\phi`   - rotation around vertical, bending on the xy plane;
    \end{equation}

    Parameters
    ----------
    L : float
        Element length [m].
    id : float
        Inner diameter of the element [m].
    od : float
        Outer diameter of the element [m].
    material : rotor-dynamics.material
        Shaft material.
    alpha : float
        Proportional damping coefficient, associated to the element Mass matrix
    beta : float
        Proportional damping coefficient, associated to the element Stiffness matrix

    Returns
    -------
    A 4 degrees of freedom shaft element, with available gyroscopic, shear and rotary inertia effects.

    Attributes
    ----------
    I : float
       Area Moment of Inertia for the element [rad.(m**4)].
    J : float
       Polar moment of inertia for the element [rad.(m**4)].
    S : float
       Cross section area for the element [rad.(m**2)].
    kappa : float
        Shear coefficient for the element, determined from :cite:`Hutchingson2001` formulation.

    References
    ----------
    .. bibliography:: ../../../docs/refs.bib

    Examples
    --------
    >>> import lmest_rotor as lm
    >>> from lmest_rotor import Material
    >>> steel = Material.from_data("steel")
    >>> Shaft_Element = lm.ShaftElement4DoF(
    ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
    ...                         )
    >>> Shaft_Element.I
    4.6019423636569244e-06
    """

    # @check_units
    def __init__(
        self,
        L,
        id,
        od,
        material,
        alpha,
        beta,
    ):
        self.DOF = 4
        if type(material) is str:
            self.material = Material.from_data(material)
        else:
            self.material = material

        self.L = L
        self.id = id
        self.od = od
        self.color = self.material.pcolor
        self.alpha = alpha
        self.beta = beta

        # Timoshenko kappa factor determination, based on the diameters relation
        if self.__is_circular():  # full circular cross- shaft
            r = (id) / (od)
            r2 = r * r
            r12 = (1 + r2) ** 2
            kappa = (
                6
                * r12
                * (
                    (1 + self.material.nu)
                    / (
                        r12 * (7 + 6 * self.material.nu)
                        + r2 * (20 + 12 * self.material.nu)
                    )
                )
            )
        elif self.__is_thickwall():  # thick-walled cross-section tube shaft
            a = self.id
            b = self.od
            v = self.material.nu
            kappa = (6 * (a ** 2 + b ** 2) ** 2 * (1 + v) ** 2) / (
                7 * a ** 4
                + 34 * a ** 2 * b ** 2
                + 7 * b ** 4
                + v * (12 * a ** 4 + 48 * a ** 2 * b ** 2 + 12 * b ** 4)
                + v ** 2 * (4 * a ** 4 + 16 * a ** 2 * b ** 2 + 4 * b ** 4)
            )
        else:  # thin-walled cross-section tube shaft
            kappa = (1 + self.material.nu) / (2 + self.material.nu)

        self.kappa = kappa

        # Other auxiliary variables - cross-section area and inertia moments
        self.S = np.pi * ((self.od / 2) ** 2 - (self.id / 2) ** 2)
        self.I = np.pi / 4 * ((self.od / 2) ** 4 - (self.id / 2) ** 4)
        self.J = np.pi / 2 * ((self.od / 2) ** 4 - (self.id / 2) ** 4)

    def __is_circular(self):
        # conditional for full circular cross-section
        return self.id == 0

    def __is_thickwall(self):
        # conditional for thick-walled cross-section
        p = (self.od - self.id) / self.od
        return p >= 0.2

    def save(self):
        # saving data function
        pass

    def load(self):
        # loading data function
        pass

    def M(self):
        r"""Mass matrix for an instance of a 4 DoF shaft element.

        Returns
        -------
        M: np.ndarray
            Mass matrix for the 4 DoF shaft element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement4DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.M()[:8, :8]
        array(8x8)
        """

        # temporary material and geometrical constants
        L = self.L

        # element level matrix declaration

        aux1 = self.material.rho * self.S * L / 420
        aux2 = self.material.rho * self.I / (30 * L)
        # fmt: off

        # Standard mass matrix
        M = aux1 * np.array([
            [   156,      0,       0,   -22*L,    54,      0,        0,    13*L  ],
            [     0,    156,    22*L,       0,     0,     54,    -13*L,       0  ],
            [     0,   22*L,   4*L**2,       0,     0,    13*L,  -3*L**2,       0],
            [ -22*L,      0,       0,   4*L**2, -13*L,       0,       0,  -3*L**2],
            [    54,      0,       0,   -13*L,   156,       0,       0,    22*L  ],
            [     0,     54,    13*L,       0,     0,     156,   -22*L,       0  ],
            [     0,  -13*L,  -3*L**2,       0,     0,   -22*L,   4*L**2,       0],
            [  13*L,      0,       0,  -3*L**2,  22*L,       0,       0,   4*L**2],
        ])

        # Secondary inertias mass matrix
        Ms = aux2 * np.array([
            [    36,     0,       0,   -3*L,    -36,      0,        0,    -3*L  ],
            [     0,    36,     3*L,      0,      0,    -36,      3*L,       0  ],
            [     0,   3*L,   4*L**2,      0,      0,   -3*L,     -L**2,       0],
            [  -3*L,     0,       0,  4*L**2,    3*L,      0,        0,    -L**2],
            [   -36,     0,       0,    3*L,     36,      0,        0,     3*L  ],
            [     0,   -36,    -3*L,      0,      0,     36,     -3*L,       0  ],
            [     0,   3*L,    -L**2,      0,      0,   -3*L,    4*L**2,       0],
            [  -3*L,     0,       0,   -L**2,    3*L,      0,        0,   4*L**2],
        ])

        # fmt: on

        M = M + Ms

        return M

    def K(self):
        r"""Stiffness matrix for an instance of a 4 DoF shaft element.

        Returns
        -------
        K: np.ndarray
            Omega independent stiffness matrix for the 4 DoF shaft element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement4DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.K()[:8, :8]
        array(8x8)
        """

        # temporary material and geometrical constants, determined as mean values
        # from the left and right radii of the taperad shaft
        L = self.L

        # temporary variables
        A = (
            12
            * self.material.E
            * self.I
            / (self.material.G * self.kappa * self.S * L ** 2)
        )

        # auxiliary variable
        aux = self.material.E * self.I / ((1 + A) * L ** 3)

        # fmt: off

        # pure stiffness matrix [Kc].
        K = aux * np.array([
            [   12,      0,          0,       -6*L,     -12,       0,          0,      -6*L ],
            [    0,     12,        6*L,          0,       0,     -12,        6*L,         0 ],
            [    0,    6*L, (4+A)*L**2,          0,       0,    -6*L, (2-A)*L**2,         0 ],
            [ -6*L,      0,          0, (4+A)*L**2,     6*L,       0,          0, (2-A)*L**2],
            [  -12,      0,          0,        6*L,      12,       0,          0,       6*L ],
            [    0,    -12,       -6*L,          0,       0,      12,       -6*L,         0 ],
            [    0,    6*L, (2-A)*L**2,          0,       0,    -6*L, (4+A)*L**2,         0 ],
            [ -6*L,      0,          0, (2-A)*L**2,     6*L,       0,          0, (4+A)*L**2],
        ])

        # fmt: on

        return K

    def Kst(self):
        """Dynamic stiffness matrix for an instance of a 4 DoF shaft element.

        Returns
        -------
        Kst: np.ndarray
            Dynamic stiffness matrix for the 4 DoF shaft element. This is
            directly dependent on the rotation speed Omega. It needs to be
            multiplied by the adequate Omega value when used in time depen-
            dent analyses. The matrix multiplier term is:

            [(Iz*Omega*rho)/(15*L)] * [Kst]

            and here the Omega value has been suppressed and must be added
            in the adequate analyses.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement4DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.Kst()[:8, :8]
        array(8x8)
        """

        # temporary material and geometrical constants, determined as mean values
        # from the left and right radii of the taperad shaft
        L = self.L

        # Auxiliary variable
        aux = self.material.rho * self.I / (15 * L)

        # fmt: off

        # dynamic stiffening matrix
        Kst = aux * np.array([
            [0,    -36,     -3*L,    0,      0,      36,     -3*L,    0],
            [0,      0,        0,    0,      0,       0,        0,    0],
            [0,      0,        0,    0,      0,       0,        0,    0],
            [0,    3*L,   4*L**2,    0,      0,    -3*L,    -L**2,    0],
            [0,     36,      3*L,    0,      0,     -36,      3*L,    0],
            [0,      0,        0,    0,      0,       0,        0,    0],
            [0,      0,        0,    0,      0,       0,        0,    0],
            [0,    3*L,    -L**2,    0,      0,    -3*L,   4*L**2,    0],
        ])

        # fmt: on

        return Kst

    def Kf(self):
        """Stiffness matrix due to the axial force aplied for an instance of a 4 DoF disk element.

        Returns
        -------
        K : np.ndarray
            A matrix of floats containing the values of the stiffness matrix.
            Obs. This method is only present here so as not to conflict
            with the abstract class.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement4DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.Kf()[:8, :8]
        array(8x8)
        """

        Kf = np.zeros((8, 8))

        return Kf

    def Kt(self):
        """Stiffness matrix due to the torque aplied for an instance of a 4 DoF disk element.

        Returns
        -------
        K : np.ndarray
            A matrix of floats containing the values of the stiffness matrix.
            Obs. This method is only present here so as not to conflict
            with the abstract class.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement4DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.Kt()[:8, :8]
        array(8x8)
        """

        Kt = np.zeros((8, 8))

        return Kt

    def C(self):
        r"""Proportional damping matrix for an instance of a 6 DoF shaft element.

        Returns
        -------
        C: np.ndarray
            Proportional damping matrix for the 6 DoF shaft element.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement4DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.C()[:8, :8]
        array(8x8)
        """

        # Proportinal damping matrix
        C = self.alpha * self.M() + self.beta * self.K()

        return C

    def G(self):
        r"""Gyroscopic matrix for an instance of a 4 DoFs shaft element.

        Returns
        -------
        G: np.ndarray
            Gyroscopic matrix for the 4 DoF shaft element. Similar to the Kst
            stiffness matrix, this Gyro matrix is also multiplied by the value
            of the rotating speed Omega. It is omitted from this and must be
            added in the respective analyses.

        Examples
        --------
        >>> import lmest_rotor as lm
        >>> from lmest_rotor import Material
        >>> steel = Material.from_data("steel")
        >>> Shaft_Element = lm.ShaftElement4DoF(
        ...                         material=steel, L=0.5, id=0, od=0.1, alpha=1, beta=1.0e-5
        ...                         )
        >>> Shaft_Element.G()[:8, :8]
        array(8x8)
        """

        L = self.L

        A = (
            12
            * self.material.E
            * self.I
            / (self.material.G * self.kappa * self.S * L ** 2)
        )

        gcor = (6 / 5) / (L ** 2 * (1 + A) ** 2)
        hcor = -(1 / 10 - 1 / 2 * A) / (L * ((1 + A) ** 2))
        icor = (2 / 15 + 1 / 6 * A + 1 / 3 * A ** 2) / ((1 + A) ** 2)
        jcor = -(1 / 30 + 1 / 6 * A - 1 / 6 * A ** 2) / ((1 + A) ** 2)

        # fmt: off

        nncor = np.array([
            [     0,  gcor,      hcor,      0],
            [ -gcor,     0,         0,   hcor],
            [ -hcor,     0,         0,   icor],
            [     0, -hcor,     -icor,      0],
        ])
        
        nn1cor = np.array([
            [    0, -gcor,   hcor,    0],
            [ gcor,     0,      0, hcor],
            [ hcor,     0,      0, jcor],
            [    0,  hcor,  -jcor,    0],
        ])
            
        mmcor = np.array([
            [     0, -gcor,    -hcor,      0],
            [  gcor,     0,        0,  -hcor],
            [ -hcor,     0,        0,   jcor],
            [     0, -hcor,    -jcor,      0],
        ])

        mm1cor = np.array([
            [     0,  gcor,  -hcor,     0],
            [ -gcor,     0,      0, -hcor],
            [  hcor,     0,      0,  icor],
            [     0,  hcor,  -icor,     0],
        ])

        aux = 2 * self.material.rho * L * self.I

        # Gyroscopic effect matrix

        hor1 = np.hstack([ -nncor, -nn1cor])
        hor2 = np.hstack([ -mmcor, -mm1cor])
        G = aux * np.vstack([hor1, hor2])
        # G = aux * np.array([
        #     [ -nncor, -nn1cor],
        #     [ -mmcor, -mm1cor],
        # ])

        # fmt: on

        return G
